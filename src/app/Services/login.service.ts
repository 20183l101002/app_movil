import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Authmodule } from '../models/Auth.module';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  api_token: any;
  url = 'http://127.0.0.1:8000/api/';
  
  constructor(private http: HttpClient, private storage: Storage) {}

   getUsers(datos: Authmodule) {
      return this.http.post(`${ this.url }login`, datos);
    } 

    getToken(){
      if (localStorage.getItem('token')) {
        this.api_token = localStorage.getItem('token');
      }else{
        this.api_token = null;
      }
      return this.api_token;
    }
    

   Guardar_token(token: string) {
    localStorage.setItem('token',token);
   }
   /*getAlbums(token: string) {
   return this.http.get(`http://localhost:3000/albums?token=${token}`);
   }*/
   borrar_token(token: string) {
    this.api_token = token;
    this.storage.remove('token');
    }
    getFitnes(token: any) {
      let cabecera = new HttpHeaders({'Authorization':`Bearer ${token}`});
    //  cabecera.set('Authorization',`Bearer ${token}` );
      
      console.log(cabecera);
      return this.http.get(`https://www.googleapis.com/fitness/v1/users/me/dataSources`, {
        headers: cabecera
      });
    } 
}