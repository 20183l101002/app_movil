import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class DietaService {
  
  url:string = 'http://127.0.0.1:8000/api/';
  constructor(private http: HttpClient, private token: LoginService) {}

list(){
  return this.http.get(`${ this.url }subsistence?token=${this.token.getToken()}`);
  
}

}
