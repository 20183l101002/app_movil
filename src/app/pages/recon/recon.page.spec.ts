import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReconPage } from './recon.page';

describe('ReconPage', () => {
  let component: ReconPage;
  let fixture: ComponentFixture<ReconPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReconPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReconPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
