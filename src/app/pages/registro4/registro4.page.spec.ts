import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Registro4Page } from './registro4.page';

describe('Registro4Page', () => {
  let component: Registro4Page;
  let fixture: ComponentFixture<Registro4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Registro4Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Registro4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
