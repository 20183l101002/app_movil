import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CambioCPage } from './cambio-c.page';

const routes: Routes = [
  {
    path: '',
    component: CambioCPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CambioCPageRoutingModule {}
