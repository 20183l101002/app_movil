import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CambioCPageRoutingModule } from './cambio-c-routing.module';

import { CambioCPage } from './cambio-c.page';
import { ComponentsModule } from 'src/app/components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CambioCPageRoutingModule,
    ComponentsModule
  ],
  declarations: [CambioCPage]
})
export class CambioCPageModule {}
