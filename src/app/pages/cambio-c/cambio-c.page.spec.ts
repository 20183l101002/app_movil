import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CambioCPage } from './cambio-c.page';

describe('CambioCPage', () => {
  let component: CambioCPage;
  let fixture: ComponentFixture<CambioCPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambioCPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CambioCPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
