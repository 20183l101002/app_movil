import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarNotaPageRoutingModule } from './agregar-nota-routing.module';

import { AgregarNotaPage } from './agregar-nota.page';
import { ComponentsModule } from 'src/app/components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarNotaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [AgregarNotaPage]
})
export class AgregarNotaPageModule {}
