import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OxigenacionSanguineaPageRoutingModule } from './oxigenacion-sanguinea-routing.module';

import { OxigenacionSanguineaPage } from './oxigenacion-sanguinea.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OxigenacionSanguineaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [OxigenacionSanguineaPage]
})
export class OxigenacionSanguineaPageModule {}
