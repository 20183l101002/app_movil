import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OxigenacionSanguineaPage } from './oxigenacion-sanguinea.page';

describe('OxigenacionSanguineaPage', () => {
  let component: OxigenacionSanguineaPage;
  let fixture: ComponentFixture<OxigenacionSanguineaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OxigenacionSanguineaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OxigenacionSanguineaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
