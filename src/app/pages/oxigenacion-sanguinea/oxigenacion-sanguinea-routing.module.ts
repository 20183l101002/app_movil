import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OxigenacionSanguineaPage } from './oxigenacion-sanguinea.page';

const routes: Routes = [
  {
    path: '',
    component: OxigenacionSanguineaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OxigenacionSanguineaPageRoutingModule {}
