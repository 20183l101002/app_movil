import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaexpedientesPage } from './listaexpedientes.page';

const routes: Routes = [
  {
    path: '',
    component: ListaexpedientesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaexpedientesPageRoutingModule {}
