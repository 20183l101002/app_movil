import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaexpedientesPageRoutingModule } from './listaexpedientes-routing.module';

import { ListaexpedientesPage } from './listaexpedientes.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListaexpedientesPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ListaexpedientesPage]
})
export class ListaexpedientesPageModule {}
