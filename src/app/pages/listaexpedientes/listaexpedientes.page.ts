import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Componente } from '../interfaces/interface';

@Component({
  selector: 'app-listaexpedientes',
  templateUrl: './listaexpedientes.page.html',
  styleUrls: ['./listaexpedientes.page.scss'],
})
export class ListaexpedientesPage implements OnInit {

componentes: Componente[] = []

  constructor( private menuCtrl: MenuController) { }

  ngOnInit() {
  }

  toggleMenu(){
    this.menuCtrl.toggle();
  }

}
