import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { Authmodule } from 'src/app/models/Auth.module';
import { LoginService } from 'src/app/Services/login.service';
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: any;
  password: any;
  loading: any;
  auth: Authmodule = new Authmodule;

  constructor(private authService: SocialAuthService,private service: LoginService,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute) {}

     fbLoginOptions = {
      scope: 'email'
    }; // 

     googleLoginOptions = {
      scope: 
        'profile email',
    
    };
ngOnInit() {}

async presentAlert() {
const alert = await this.alertController.create({
header: 'Error',
subHeader: 'Datos incorrectos',
message: 'Verifica tus datos',
buttons: [{
    text: 'Aceptar'
}
]});
await alert.present();
}
async login(form: NgForm) {
this.loading = await this.loadingController.create({
message: 'Espere',
spinner: 'bubbles',
mode: 'ios',
});
this.loading.present();

// tslint:disable-next-line: deprecation
this.service.getUsers(this.auth).subscribe(rep => {
// tslint:disable-next-line: triple-equals
console.log(rep);
if (rep != '') {
this.loading.dismiss();
this.service.Guardar_token(rep['token']);
this.router.navigateByUrl('tabs');
} else {
this.loading.dismiss();
this.presentAlert();
}
});
}
signInWithGoogle(): void {
  this.authService.signIn(GoogleLoginProvider.PROVIDER_ID,this.googleLoginOptions).then(res => {
    console.log(res);
    this.service.getFitnes(res.authToken).subscribe(
      res =>{
        console.log(res);
      },
      error =>{
        console.log(error);
      }
    );
  })
  .catch(err => console.error(err));
}

signInWithFB(): void {
  this.authService.signIn(FacebookLoginProvider.PROVIDER_ID,this.fbLoginOptions).then(res => console.log(res))
  .catch(err => console.error(err));
}

signOut(): void {
  this.authService.signOut();
}
}
