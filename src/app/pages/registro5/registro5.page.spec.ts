import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Registro5Page } from './registro5.page';

describe('Registro5Page', () => {
  let component: Registro5Page;
  let fixture: ComponentFixture<Registro5Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Registro5Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Registro5Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
