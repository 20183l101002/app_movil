import { RecipeService } from './../../Services/recipe.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medicamentos',
  templateUrl: './medicamentos.page.html',
  styleUrls: ['./medicamentos.page.scss'],
})
export class MedicamentosPage implements OnInit {
  recipes:any;
  inquiries:any;
  constructor(private recipe: RecipeService) { }

  ngOnInit() {
  
    this.recipe.list().subscribe(
      resp => {
        this.recipes = resp;
        console.log(this.recipes);
      }
    );
    this.recipe.getinquieries().subscribe(
      resp => {
        this.inquiries = resp;
        console.log(this.inquiries);
      }
    );
  }
  returnRecipe(varia){
   console.log(varia);
    this.recipe.returnRecipe(varia);
  }
  DownloadRecipe(varia){
    console.log(varia);
     this.recipe.DowenloadRecipe(varia);
   }
}
