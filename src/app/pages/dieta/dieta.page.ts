import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DietaService } from 'src/app/Services/dieta.service';

@Component({
  selector: 'app-dieta',
  templateUrl: './dieta.page.html',
  styleUrls: ['./dieta.page.scss'],
})
export class DietaPage implements OnInit {
  dietas:any;
  constructor(private dieta: DietaService, private route: ActivatedRoute) { }

  ngOnInit() {
  this.dieta.list().subscribe(
  resp => {
  this.dietas = resp;
  console.log(this.dietas);
  }
  );
  }
}
