import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PresionArterialPageRoutingModule } from './presion-arterial-routing.module';

import { PresionArterialPage } from './presion-arterial.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PresionArterialPageRoutingModule,
    ComponentsModule
  ],
  declarations: [PresionArterialPage]
})
export class PresionArterialPageModule {}
