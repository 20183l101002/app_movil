import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PresionArterialPage } from './presion-arterial.page';

describe('PresionArterialPage', () => {
  let component: PresionArterialPage;
  let fixture: ComponentFixture<PresionArterialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresionArterialPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PresionArterialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
